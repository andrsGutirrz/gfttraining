﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shape
{
    public abstract class Shape
    {
        public string name { get; set; }

        public virtual void getInfo() {
            Console.WriteLine($"Name: {name}");
        }

        public abstract double area();
    }

    public class Circle : Shape
    {
        public double radius { get; set; }

        public Circle(double radius) {
            this.radius = radius;
        }

        public override double area()
        {
            return Math.PI*(this.radius* this.radius);
        }

        public override void getInfo()
        {
            base.getInfo();
            Console.WriteLine($"Circle with area: {this.area()} ");
        }

    }

    public class Rectangle : Shape
    {
        public double lengh { get; set; }
        public double width { get; set; }

        public Rectangle(double lengh, double width)
        {
            this.lengh = lengh;
            this.width = width;
        }

        public override double area() => lengh * width;

        public override void getInfo()
        {
            base.getInfo();
            Console.WriteLine($"Rectangle with area: {this.area()} ");
        }
    }
}
