using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MethodOverriding;
using IntControlRemote;
using Shape;

namespace Design_patterns_lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Method Overriding Example
            Boy boy = new Boy();
            boy.eat();

            //Interface
            ControlRemote ctrlRmt = new ControlRemote();
            ctrlRmt.on();
            ctrlRmt.off();

            //Abstract Class
            Circle circle = new Circle(5);
            Console.WriteLine( circle.area());

            Rectangle rectangle = new Rectangle(5,5);
            Console.WriteLine(rectangle.area());

            //console pause
            Console.ReadKey();
        }
    }
}
