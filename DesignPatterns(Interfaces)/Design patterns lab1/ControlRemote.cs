﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntControlRemote
{
    public interface IControlRemote
    {
        void on();
        void off();
    }

    public class ControlRemote : IControlRemote
    {
        public void off()
        {
            Console.WriteLine("turning off");
        }

        public void on()
        {
            Console.WriteLine("turning on");
        }
    }

}
