﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodOverriding
{
    public class Human
    {
        public virtual void eat() {
            Console.WriteLine("Eating as a Human!");
        }

    }

    public class Boy : Human
    {
        public override void eat()
        {
            Console.WriteLine("Eating as a Boy!");
        }

    }
}
