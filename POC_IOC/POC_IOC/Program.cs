﻿using System;
using Unity;
using Unity.Injection;

namespace POC_IOC
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("&&&&&&&&&| poc1 | &&&&&&&&&");
            //poc1();
            Console.WriteLine("&&&&&&&&&| poc2 | &&&&&&&&&");
            poc2();
            Console.WriteLine("&&&&&&&&&| poc2 | &&&&&&&&&");
            poc3();

            Console.ReadLine();
        }

        public static void poc1() {
            IUnityContainer container = new UnityContainer();
            container.RegisterType<ICar, BMW>();

            container.RegisterType<ICar, BMW>();
            container.RegisterType<ICar, Audi>("LuxuryCar");

            // Register Driver type            
            container.RegisterType<Driver>("LuxuryCarDriver",
                            new InjectionConstructor(container.Resolve<ICar>("LuxuryCar")));

            Driver driver1 = container.Resolve<Driver>();// injects BMW
            driver1.RunCar();

            Driver driver2 = container.Resolve<Driver>("LuxuryCarDriver");// injects Audi
            driver2.RunCar();
        }
        public static void poc2() {
            var container = new UnityContainer();
            ICar audi = new Audi();
            container.RegisterInstance<ICar>(audi);

            Driver driver1 = container.Resolve<Driver>();
            driver1.RunCar();
            driver1.RunCar();

            Driver driver2 = container.Resolve<Driver>();
            driver2.RunCar();
        }
        public static void poc3()
        {
            var container = new UnityContainer();

            container.RegisterType<ICar, Audi>();
            container.RegisterType<ICarKey, AudiKey>();

            var driver = container.Resolve<Driver>();//aca usamos el constructor
            driver.RunCar();
        }
    }

    public interface ICarKey
    {

    }

    public class BMWKey : ICarKey
    {

    }

    public class AudiKey : ICarKey
    {

    }

    public class FordKey : ICarKey
    {

    }
    public interface ICar
    {
        int Run();
    }

    public class BMW : ICar
    {
        private int _miles = 0;

        public int Run()
        {
            return ++_miles;
        }
    }

    public class Ford : ICar
    {
        private int _miles = 0;

        public int Run()
        {
            return ++_miles;
        }
    }

    public class Audi : ICar
    {
        private int _miles = 0;

        public int Run()
        {
            return ++_miles;
        }

    }
    public class Driver
    {
        private ICar _car = null;
        private ICarKey _key = null;

        [InjectionConstructor]
        public Driver(ICar car)
        {
            _car = car;
            
        }

        [InjectionMethod]
        public void insertCar(ICar mycar) {
            this._car = mycar;
        }



        public void RunCar()
        {
            Console.WriteLine($"Running {(_car.GetType().Name)}  - {( _car.Run())} mile ");
        }
    }
}
