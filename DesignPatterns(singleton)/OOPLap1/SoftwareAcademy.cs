﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareAcademy
{
    public interface ITeacher
    {
        string Name { get; set; }
        void AddCourse(ICourse course);
        string toString();
    }

    public interface ICourse
    {
        string Name { get; set; }
        ITeacher Teacher { get; set; }
        void AddTopic(string topic);
        string toString();
    }

    public interface ILocalCourse: ICourse
    {
        string Lab { get; set;  }
    }

    public interface IOffsiteCourse: ICourse
    {
        string Town { get; set; }
    }

    public interface ICourseFactory
    {
        //the methods can be void type, and every time we create an object, we insert it into a list
        ITeacher CreateTeacher(string name);
        ILocalCourse CreateLocalCourse(string name, ITeacher teacher, string lab);
        IOffsiteCourse CreateOffsiteCourse(string name, ITeacher teacher, string town);
    }

    public class CourseFactory : ICourseFactory
    {
        // Singleton
        private static CourseFactory _instance;
        private CourseFactory()
        {
            Console.WriteLine("Building a CourseFactory man! ");
        }
        /* not thread safe
        public static CourseFactory Instance() {
            if (_instance == null)
            {
                _instance = new CourseFactory();
            }
            return _instance;
        }
        */
        // thread safe
        public static CourseFactory Instance
        {
            get { return Nested.instance; }
        }

        private class Nested
        {
            static Nested() { }

            internal static readonly CourseFactory instance = new CourseFactory();
        }


        public ILocalCourse CreateLocalCourse(string name, ITeacher teacher, string lab)
        {
            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(lab)) {
                throw new ArgumentNullException("There is a parameter which is not with content! watch out!");
            }
            return new LocalCourse(name, teacher, lab);
        }

        public IOffsiteCourse CreateOffsiteCourse(string name, ITeacher teacher, string town)
        {
            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(town))
            {
                throw new ArgumentNullException("There is a parameter which is not with content! watch out!");
            }
            return new OffsiteCourse(name,teacher,town);
        }

        public ITeacher CreateTeacher(string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("There is a parameter which is not with content! watch out!");
            }
            return new Teacher(name);
        }
    }

    // Implementing interfaces

    public class Teacher : ITeacher
    {
        // variables
        List<ICourse> courses;
        public Teacher(string name)
        {
            this.Name = name;
            this.courses = new List<ICourse>();
        }
        public string Name { get; set; }

        public void AddCourse(ICourse course)
        {
            this.courses.Add(course);
        }

        public string toString()
        {
            string output = "";
            output = String.Format("Teacher: Name = ({0}); ",this.Name);
            output = output + String.Format("Courses =[( "); // course names –comma separated)]");
            foreach (var item in this.courses)
            {
                output = output + String.Format("{0}, ",item.Name);
            }
            output = output + String.Format(")] ");
            return output;
        }
    }
    public class LocalCourse : ILocalCourse
    {
        //variables
        List<string> topics;

        //constructor
        public LocalCourse(string name, ITeacher teacher, string lab)
        {
            this.Name = name;
            this.Teacher = teacher;
            this.Lab = lab;
            topics = new List<string>();

            this.Teacher.AddCourse(this); //agregamos el curso
        }

        public string Lab { get; set; }
        public string Name { get; set; }
        public ITeacher Teacher { get; set; }

        public void AddTopic(string topic)
        {
            this.topics.Add(topic);
        }

        public string toString() {
            string output = "";
            output = String.Format("(LocalCourse): Name = ({0}); Teacher = {1}; ", this.Name, this.Teacher.Name);
            output = output + String.Format("Topics =[( "); // course names –comma separated)]");
            foreach (var item in this.topics)
            {
                output = output + String.Format("{0}, ", item);
            }
            output = output + String.Format(" )];");
            output = output + String.Format(" Lab= {0}", this.Lab );

            return output;
        }
    }
    public class OffsiteCourse : IOffsiteCourse
    {
        //variables
        List<string> topics;

        //constructor
        public OffsiteCourse(string name, ITeacher teacher, string town)
        {
            this.Name = name;
            this.Teacher = teacher;
            this.Town = town;
            topics = new List<string>();

            this.Teacher.AddCourse(this); //agregamos el curso
        }

        public string Town { get; set; }
        public string Name { get; set; }
        public ITeacher Teacher { get; set; }

        public void AddTopic(string topic)
        {
            this.topics.Add(topic);
        }

        public string toString() {
            string output = "";
            output = String.Format("(Offsite): Name = ({0}); Teacher = {1}; ", this.Name, this.Teacher.Name);
            output = output + String.Format("Topics =[( "); // course names –comma separated)]");
            foreach (var item in this.topics)
            {
                output = output + String.Format("{0}, ", item);
            }
            output = output + String.Format(" )];");
            output = output + String.Format(" Town= {0}", this.Town );

            return output;
        }
    }

} // end  namespace SoftwareAcademy

