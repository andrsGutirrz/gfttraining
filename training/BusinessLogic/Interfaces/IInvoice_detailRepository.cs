﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IInvoice_detailRepository
    {
        List<Invoice_detail> GetInvoice_detail();
    }
}
