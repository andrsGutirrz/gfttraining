﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class Invoice_detail
    {
        /*
        	[INVOICE_DETAIL_ID] [int] IDENTITY(1,1) NOT NULL,
	        [INVOICE_ID] [int] NOT NULL,
	        [PRODUCT_ID] [int] NOT NULL,
	        [AMOUNT] [int] NOT NULL,
	        [PRICE] [decimal](18, 0) NOT NULL, 
        */
        public int invoice_detail_id { get; set; }
        public int invoice_id { get; set; }
        public int product_id { get; set; }
        public decimal price { get; set; }

    }
}
