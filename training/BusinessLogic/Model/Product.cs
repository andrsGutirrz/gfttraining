﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class Product
    {
        public int product_id { get; set; }
        public string description { get; set; }
        public decimal price { get; set; }
        public DateTime Date { get; set; }
    }
}
