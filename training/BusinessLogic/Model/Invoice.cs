﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class Invoice
    {
        /*
         	[INVOICE_ID] [int] IDENTITY(1,1) NOT NULL,
	        [CUSTOMER_ID] [int] NOT NULL,
	        [DATE] [date] NULL,
	        [TOTAL_AMOUNT] [decimal](18, 0) NOT NULL,         
        */
        public int invoice_id { get; set; }
        public int customer_id { get; set; }
        public DateTime Date { get; set; }
        public decimal total_amount  {get; set;}

    }
}
