﻿using BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Important
using Dapper;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogic
{
    public class ProducteRepository : IProductRepository
    {
        private IDbConnection db = new SqlConnection(
         ConfigurationManager.ConnectionStrings["Training"].ConnectionString);
        public List<Product> GetProducts()
        {
            var spName = "GETPRODUCTS";
            return db.Query<Product>(spName,CommandType.StoredProcedure).ToList();
        }
    }
}
