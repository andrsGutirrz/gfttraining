﻿using BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Important
using Dapper;

namespace BusinessLogic
{
    public class CustomerRepository : ICustomerRepository
    {
        private IDbConnection db = new SqlConnection(
            ConfigurationManager.ConnectionStrings["Training"].ConnectionString);
        public List<Customer> GetCustomer()
        {
            var sqlString = "select * from TRN_CUSTOMER";
            return this.db.Query<Customer>(sqlString).ToList();
        }
    }
}
