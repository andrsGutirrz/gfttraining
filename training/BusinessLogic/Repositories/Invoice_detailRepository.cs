﻿using BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace BusinessLogic.Repositories
{
    public class Invoice_detailRepository : IInvoice_detailRepository
    {
        private IDbConnection db = new SqlConnection(
        ConfigurationManager.ConnectionStrings["Training"].ConnectionString);
        public List<Invoice_detail> GetInvoice_detail()
        {
            var sqlString = "select * from INVOICE_DETAIL";
            return this.db.Query<Invoice_detail>(sqlString).ToList();
        }
    }
}
