﻿using BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace BusinessLogic.Repositories
{
    public class InvoiceRepository : IInvoiceRepository
    {

        private IDbConnection db = new SqlConnection(
   ConfigurationManager.ConnectionStrings["Training"].ConnectionString);

        public List<Invoice> GetInvoice()
        {
            var sqlString = "select * from TRN_INVOICE";
            return this.db.Query<Invoice>(sqlString).ToList();
        }

    }
}
