﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BusinessLogic;

namespace WCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ITrainingService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ITrainingService.svc or ITrainingService.svc.cs at the Solution Explorer and start debugging.
    public class ITrainingService : IITrainingService
    {
        public void DoWork()
        {
        }

        public List<Customer> GetCustomer()
        {
            var customer = new CustomerRepository();
            return customer.GetCustomer();
        }
    }
}
