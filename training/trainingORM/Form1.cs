﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using BusinessLogic;
using BusinessLogic.Repositories;

namespace trainingORM
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var customer = new CustomerRepository();
            var products = new ProducteRepository();
            var invoices = new InvoiceRepository();
            var invoiceDetail = new Invoice_detailRepository();
            //
            this.comboBox_customer.DataSource = customer.GetCustomer();
            comboBox_customer.DisplayMember = "Name";
            comboBox_customer.ValueMember = "Customer_Id";
            //
            this.comboBox_product.DataSource = products.GetProducts();
            comboBox_product.DisplayMember = "description";
            comboBox_product.ValueMember = "product_id";
            //
            this.dataGridViewInvoice.DataSource = invoices.GetInvoice();
            //
            this.dataGridViewInvoiceDetail.DataSource = invoiceDetail.GetInvoice_detail();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dataGridViewInvoice_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Console.WriteLine(e.ColumnIndex);
        }
    }
}
