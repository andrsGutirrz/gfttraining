using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftwareAcademy;

namespace OOPLap1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ICourseFactory factory = new CourseFactory();
                var teacher = (Teacher)factory.CreateTeacher("Andres");
                var course1 = (LocalCourse)factory.CreateLocalCourse("", teacher, "truth tables"); //throws an exception!
                Console.WriteLine(teacher.toString());
                Console.WriteLine(course1.toString());
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("{0} Exception caught.", e);
            }
            Console.ReadKey();

        }
    }
}


/*without factory pattern
//Creating a teacher
Teacher t = new Teacher("Andres");
Console.WriteLine(t.toString());
//creating a localcourse
LocalCourse lc = new LocalCourse("Math 1",t,"Math");
LocalCourse lc2 = new LocalCourse("Math 2",t,"Math");
OffsiteCourse oc = new OffsiteCourse("Math 3",t,"Math");
Console.WriteLine(lc.toString());
oc.AddTopic("topic1");
oc.AddTopic("topic2");
oc.AddTopic("topic3");
Console.WriteLine(oc.toString());
Console.WriteLine(t.toString());
*/
//pause console