using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OOPLap1.implemented;
using SoftwareAcademy;

namespace OOPLap1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ICourseFactory factory = CourseFactory.Instance;
                var teacher = (Teacher)factory.CreateTeacher("Andres");
                var course1 = (LocalCourse)factory.CreateLocalCourse("math", teacher, "truth tables"); //throws an exception!
                course1.addSignature(new Topic(3));
                course1.addSignature(new Topic(3));
                course1.addSignature(new Topic(2));
                course1.addSignature(new Topic(2));
                Console.WriteLine(course1.totalHours());
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("{0} Exception caught.", e);
            }
            Console.ReadKey();

        }
    }
}
