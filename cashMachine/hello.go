package main

import (
	"fmt"
	"time"
  "math"
  "math/rand"
)

func main() {
	fmt.Println("Welcome to the playground!")
  fmt.Println("My favorite number is", rand.Intn(10))
	fmt.Println("The time is", time.Now())
  fmt.Printf("Now you have %g problems.\n", math.Sqrt(7))
}
