using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OOPLap1.implemented;
using SoftwareAcademy;

namespace OOPLap1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ICourseFactory factory = CourseFactory.Instance;
                var teacher = (Teacher)factory.CreateTeacher("Andres");
                var course1 = (LocalCourse)factory.CreateLocalCourse("math", teacher, "truth tables"); //throws an exception!
                course1.addSignature(new Topic(3));
                course1.addSignature(new Topic(3));
                course1.addSignature(new Topic(2));
                course1.addSignature(new Topic(2));
                Console.WriteLine(course1.totalHours());

                Console.WriteLine("*** Testing Chain responsability ***");

                //creating actors
                var principal = new Principal();
                var regionSupervisor = new RegionSupervisor();
                var justice = new Justice();

                //setting their tolerance against a problem
                teacher.setCapability("bullied");
                principal.setCapability("violence");
                regionSupervisor.setCapability("sexual");
                justice.setCapability("murdered");

                //setting their superior
                teacher
                    .setNext(principal)
                    .setNext(regionSupervisor)
                    .setNext(justice);
                /*
                teacher.setNext(principal);
                principal.setNext(regionSupervisor);
                */

                //setting the test complaints
                string complaint1 = "Melissa bullied me";
                string complaint2 = "I suffered physical violence from/by/because? Melissa";
                string complaint3 = "Profesor, there is someone that is making me feel bad, he is practicing sexual harassment";
                string complaint4 = "Flor Natalia murdered him ";

                //testing
                teacher.respond(complaint2);

            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("{0} Exception caught.", e);
            }
            Console.ReadKey();

        }
    }
}
