﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareAcademy
{
    public interface ISignature
    {
        double hours { get; set; }
        double totalHours();
    }

    public interface ITeacher
    {
        string Name { get; set; }
        void AddCourse(ICourse course);
        string toString();
    }

    public interface ICourse
    {
        string Name { get; set; }
        ITeacher Teacher { get; set; }
        void AddTopic(string topic);
        string toString();
    }

    public interface ILocalCourse : ICourse
    {
        string Lab { get; set; }
    }

    public interface IOffsiteCourse : ICourse
    {
        string Town { get; set; }
    }

    public interface ICourseFactory
    {
        //the methods can be void type, and every time we create an object, we insert it into a list
        ITeacher CreateTeacher(string name);
        ILocalCourse CreateLocalCourse(string name, ITeacher teacher, string lab);
        IOffsiteCourse CreateOffsiteCourse(string name, ITeacher teacher, string town);
    }

    public abstract class AbsComplaintsHandler
    {
        public AbsComplaintsHandler next;
        public string myCapability;

        // it returns AbsComplaintsHander to allow encadenamiento*
        public AbsComplaintsHandler setNext(AbsComplaintsHandler _next)
        {
            this.next = _next;
            return _next;
        }
        public void setCapability(string capability) => this.myCapability = capability;

        public abstract void respond(string complaint);

    }

}