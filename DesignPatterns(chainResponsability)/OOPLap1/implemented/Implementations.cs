﻿using SoftwareAcademy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPLap1.implemented
{
    public class CourseFactory : ICourseFactory
    {
        // Singleton
       // private static CourseFactory _instance;
        private CourseFactory()
        {
            Console.WriteLine("Building a CourseFactory man! ");
        }
        /* not thread safe
        public static CourseFactory Instance() {
            if (_instance == null)
            {
                _instance = new CourseFactory();
            }
            return _instance;
        }
        */
        // thread safe
        public static CourseFactory Instance
        {
            get { return Nested.instance; }
        }

        private class Nested
        {
            static Nested() { }

            internal static readonly CourseFactory instance = new CourseFactory();
        }


        public ILocalCourse CreateLocalCourse(string name, ITeacher teacher, string lab)
        {
            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(lab))
            {
                throw new ArgumentNullException("There is a parameter which is not with content! watch out!");
            }
            return new LocalCourse(name, teacher, lab);
        }

        public IOffsiteCourse CreateOffsiteCourse(string name, ITeacher teacher, string town)
        {
            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(town))
            {
                throw new ArgumentNullException("There is a parameter which is not with content! watch out!");
            }
            return new OffsiteCourse(name, teacher, town);
        }

        public ITeacher CreateTeacher(string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("There is a parameter which is not with content! watch out!");
            }
            return new Teacher(name);
        }
    }
    public class LocalCourse : ILocalCourse, ISignature
    {
        //variables
        List<string> topics;
        List<ISignature> signatures;

        //constructor
        public LocalCourse(string name, ITeacher teacher, string lab)
        {
            this.Name = name;
            this.Teacher = teacher;
            this.Lab = lab;
            topics = new List<string>();
            signatures = new List<ISignature>();
            this.Teacher.AddCourse(this); //agregamos el curso
        }

        public string Lab { get; set; }
        public string Name { get; set; }
        public ITeacher Teacher { get; set; }
        public double hours { get; set; }

        public void addSignature(ISignature signature) => this.signatures.Add(signature);

        public void AddTopic(string topic) => this.topics.Add(topic);
        public string toString()
        {
            string output = "";
            output = String.Format("(LocalCourse): Name = ({0}); Teacher = {1}; ", this.Name, this.Teacher.Name);
            output = output + String.Format("Topics =[( "); // course names –comma separated)]");
            foreach (var item in this.topics)
            {
                output = output + String.Format("{0}, ", item);
            }
            output = output + String.Format(" )];");
            output = output + String.Format(" Lab= {0}", this.Lab);

            return output;
        }

        public double totalHours() => this.signatures.Select(signature => signature.hours).Aggregate((total, item) => total + item);
    }
    public class OffsiteCourse : IOffsiteCourse,ISignature
    {
        //variables
        List<string> topics;
        List<ISignature> signatures;

        //constructor
        public OffsiteCourse(string name, ITeacher teacher, string town)
        {
            this.Name = name;
            this.Teacher = teacher;
            this.Town = town;
            topics = new List<string>();
            this.signatures = new List<ISignature>();
            this.hours = 0;
            this.Teacher.AddCourse(this); //agregamos el curso
        }

        public string Town { get; set; }
        public string Name { get; set; }
        public ITeacher Teacher { get; set; }
        public double hours { get; set; }

        public void addSignature(ISignature signature) => this.signatures.Add(signature);

        public void AddTopic(string topic) => this.topics.Add(topic);

        public string toString()
        {
            string output = "";
            output = String.Format("(Offsite): Name = ({0}); Teacher = {1}; ", this.Name, this.Teacher.Name);
            output = output + String.Format("Topics =[( "); // course names –comma separated)]");
            foreach (var item in this.topics)
            {
                output = output + String.Format("{0}, ", item);
            }
            output = output + String.Format(" )];");
            output = output + String.Format(" Town= {0}", this.Town);

            return output;
        }

        public double totalHours() => this.signatures.Select(signature => signature.hours).Aggregate((total, item) => total + item);

    }
    public class Topic : ISignature
    {
        public Topic(double hours) => this.hours = hours;
        public double hours { get; set; }
        public double totalHours() => this.hours;

    }

    //Implements AbsComplaintHandler
    public class Teacher : AbsComplaintsHandler, ITeacher
    {
        // variables
        List<ICourse> courses;
        public Teacher(string name)
        {
            this.Name = name;
            this.courses = new List<ICourse>();
        }
        public string Name { get; set; }

        public void AddCourse(ICourse course)
        {
            this.courses.Add(course);
        }

        public string toString()
        {
            string output = "";
            output = String.Format("Teacher: Name = ({0}); ", this.Name);
            output = output + String.Format("Courses =[( "); // course names –comma separated)]");
            foreach (var item in this.courses)
            {
                output = output + String.Format("{0}, ", item.Name);
            }
            output = output + String.Format(")] ");
            return output;
        }


        //from AbsComplaintsHander
        public override void respond(String complaint)
        {
            if (complaint.Contains(this.myCapability))
            {
                // is able to handle it
                Console.WriteLine("{0} could handle the complaint about \"{1}\" ", this.GetType(), complaint);
            }
            else
            {
                //isnt able to handle it
                this.next.respond(complaint);
            }
        }
    }
    public class Principal : AbsComplaintsHandler
    {
        public override void respond(string complaint)
        {
            if (complaint.Contains(this.myCapability))
            {
                // is able to handle it
                Console.WriteLine("{0} could handle the complaint about \"{1}\" ", this.GetType(), complaint);
            }
            else
            {
                //isnt able to handle it
                this.next.respond(complaint);
            }
        }
    }
    public class RegionSupervisor : AbsComplaintsHandler
    {
        public override void respond(string complaint)
        {
            if (complaint.Contains(this.myCapability))
            {
                // is able to handle it
                Console.WriteLine("{0} could handle the complaint about \"{1}\" ", this.GetType(), complaint);
            }
            else
            {
                //isnt able to handle it
                this.next.respond(complaint);
            }
        }
    }
    public class Justice : AbsComplaintsHandler
    {
        public override void respond(string complaint)
        {
            Console.WriteLine("FINALLY!! {0} could handle the complaint about \"{1}\" ", this.GetType(), complaint);
        }
    }

}
