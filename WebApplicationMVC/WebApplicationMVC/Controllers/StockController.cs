﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplicationMVC.Controllers
{
    public class StockController : Controller
    {
        // GET: Stock
        public ActionResult Quantity()
        {
            ViewBag.Message = "This is Stock module -> Quantity";

            return View();
        }
    }
}