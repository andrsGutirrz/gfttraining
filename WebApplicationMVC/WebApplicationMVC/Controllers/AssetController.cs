﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplicationMVC.Controllers
{
    public class AssetController : Controller
    {
        // GET: Asset
        public ActionResult Assets()
        {
            return View();
        }

        public ActionResult About() {
            ViewBag.Message = "You are in the about tab in Assets module";
            return View();
        }

        public ActionResult Register()
        {
            ViewBag.Message = "You are in the Register tab in Assets module";
            return View();
        }

    }
}