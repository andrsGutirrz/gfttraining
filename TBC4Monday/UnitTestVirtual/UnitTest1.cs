﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestVirtual
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            //Create
            Foo foo = new Foo();
            //expected
            string expected = "Base - goo";
            string actual = foo.goo();
            //Assert
            Assert.AreEqual(expected,actual);
        }
    }
}

/*
 
            Foo foo = new Foo();
            Woo woo = new Woo();
            Foo woo2 = new Woo();
            Moo moo = new Moo();
            Foo moo2 = new Moo();

            foo.goo();
            woo.goo();
            woo2.goo();
            moo.goo();
            moo2.goo();     
     
*/
