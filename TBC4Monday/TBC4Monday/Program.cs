﻿using System;

namespace TBC4Monday
{
    public class Foo
    {
        public virtual string goo()
        {
            return "Base - goo";
        }
    }
    public class Woo : Foo {
        public override string goo()
        {
            return "Woo derived - goo";
        }
    }
    public class Moo : Foo {
        public new string goo()
        {
            return "Moo new - goo";
        }
    }
    public class Program
    {
        static void Main(string[] args)
        {
            Foo foo = new Foo();
            Woo woo = new Woo();
            Foo woo2 = new Woo();
            Moo moo = new Moo();
            Foo moo2 = new Moo();

            Console.WriteLine(  foo.goo() );
            Console.WriteLine(  woo.goo() );
            Console.WriteLine(  woo2.goo() );
            Console.WriteLine(  moo.goo() );
            Console.WriteLine(  moo2.goo() );
                       
            Console.ReadKey();
        }
        
        ~Program() { Console.WriteLine("deleted"); }
    }


}
