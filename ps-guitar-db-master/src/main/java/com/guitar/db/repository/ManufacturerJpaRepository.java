package com.guitar.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.guitar.db.model.Manufacturer;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;


@Repository
public interface ManufacturerJpaRepository extends JpaRepository<Manufacturer, Long> {

    Manufacturer findByNameEquals(String name);
    List<Manufacturer> findByfoundedDateLessThan(Date date);

}
