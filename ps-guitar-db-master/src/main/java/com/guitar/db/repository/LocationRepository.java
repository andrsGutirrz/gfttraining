package com.guitar.db.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.guitar.db.model.Location;

@Repository
public class LocationRepository {
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private LocationJpaRepository locationJpaRepository; 
	
	/**
	 * Create
	 */
	public Location create(Location loc) {
		locationJpaRepository.saveAndFlush(loc);
		return loc;
	}

	/**
	 * Update
	 */
	public Location update(Location loc) {
		locationJpaRepository.save(loc);
		return loc;
	}

	/**
	 * Delete
	 */
	public void delete(Location loc) {
		locationJpaRepository.delete(loc);
	}

	/**
	 * Find
	 */
	public Location find(Long id) {
		return locationJpaRepository.findOne(id);
	}

	/**
	 * Custom finder
	 */
	public List<Location> getLocationByStateName(String name) {
		@SuppressWarnings("unchecked")
		List<Location> locs = locationJpaRepository.findByStateLike(name);
		return locs;
	}
}
