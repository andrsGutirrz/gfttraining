package com.guitar.db.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.guitar.db.model.Manufacturer;

@Repository
public class ManufacturerRepository {
	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private ManufacturerJpaRepository manufacturerJpaRepository;


	/**
	 * Create
	 */
	public Manufacturer create(Manufacturer man) {
		manufacturerJpaRepository.saveAndFlush(man);
		return man;
	}

	/**
	 * Update
	 */
	public Manufacturer update(Manufacturer man) {
		//in location example, they use save instead of saveAndFlush
		manufacturerJpaRepository.saveAndFlush(man);
		return man;
	}

	/**
	 * Delete
	 */
	public void delete(Manufacturer man) {
		manufacturerJpaRepository.delete(man);
	}

	/**
	 * Find
	 */
	public Manufacturer find(Long id) {

		return manufacturerJpaRepository.findOne(id);
	}

	/**
	 * Custom finder
	 */
	public List<Manufacturer> getManufacturersFoundedBeforeDate(Date date) {
		@SuppressWarnings("unchecked")
		List<Manufacturer> mans = manufacturerJpaRepository.findByfoundedDateLessThan(date);
		return mans;
	}

	/**
	 * Custom finder
	 */
	public Manufacturer getManufacturerByName(String name) {
		Manufacturer man = manufacturerJpaRepository.findByNameEquals(name);
		return man;
	}

	/**
	 * Native Query finder
	 */
	public List<Manufacturer> getManufacturersThatSellModelsOfType(String modelType) {
		@SuppressWarnings("unchecked")
		List<Manufacturer> mans = entityManager
				.createNamedQuery("Manufacturer.getAllThatSellAcoustics")
				.setParameter(1, modelType).getResultList();
		return mans;
	}
}
