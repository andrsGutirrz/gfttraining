package com.guitar.db.repository;

import com.guitar.db.model.Model;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface ModelJpaRepository extends JpaRepository<Model, Long> {
    List<Model> findByfoundedDateLessThan(Date date);

}
