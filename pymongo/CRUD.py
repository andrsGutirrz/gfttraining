from datetime import datetime
from pymongo import ReturnDocument
import connection
import utils

myconn = connection.main
myUtils = utils

""" CRUD """
def insert(doc):
    myconn().products.insert(doc)

def delete(doc):
    myconn().products.remove(doc)

def updateSingle(operator,filter,property):
    myconn().products.update_one({filter["field"]: filter["value"]},{operator: {property["field"]: property["value"]}})

def updateMultiple(operator,filter, property):
    myconn().products.update_many({filter["field"]: filter["value"]},{operator: {property["field"]: property["value"]}})

def findAll():
    return myconn().products.find()

def findByCriteria(criteria):
    return myconn().products.find_one({criteria['criteria']:criteria['value']})

def findAndModify(operator,filter, property):
    #return_document property must be set to after, otherwise the pre-update doc will be returned
    return myconn().products.find_one_and_update({filter["field"]: filter["value"]},{operator: {property["field"]: property["value"]}},return_document=ReturnDocument.AFTER)

myconn().products.delete_many({})
#Embedded documents
user_doc = {
 "username":"foouser",
 "twitter":{
    "username":"footwitter",
    "password":"secret",
    "email":"twitter@example.com"
 },
 "facebook":{
    "username":"foofacebook",
    "password":"secret",
    "email":"facebook@example.com"
 },
 "irc":{
    "username":"fooirc",
    "password":"secret",
 }
}


insert(user_doc)
print("Printing all objects from the db")
#Search using property of embedded document

results = findByCriteria({'criteria':'facebook.username','value':'foofacebook'})
#Update embedded property
updateSingle('$set',{'field':'facebook.username','value':'foofacebook'},{'field':'twitter.username','value':'newuser'})
myconn().products.delete_many({})
#One-to-many relationship
user_doc = {
 "username":"foouser",
 "emails":[
 {
 "email":"foouser1@example.com",
 "primary":True
 },
 {
 "email":"foouser2@example2.com",
 "primary":False
 },
 {
 "email":"foouser3@example3.com",
 "primary":False
 }
 ]
}

# Retrieve a doc by means of one of its emails
insert(user_doc)
result_search_email = findByCriteria({'criteria':'"emails.email','value':'foouser1@example.com'})


# Retrieve the just-inserted document via username
user_doc_result = findByCriteria({'criteria':'username','value':'foouser'})
# Remove the "foouser2@example2.com" email address sub-document from the embedded list
del user_doc_result["emails"][1]
updateMultiple('$set',{'field':'facebook.username','value':'foouser'},{'field':'emails','value':user_doc_result})
result_search_email = findAll()
for a in result_search_email:
    print (a)

# Use $pull to atomically remove the "foouser2@example2.com" email sub-document
updateSingle('$pull',{'field':'username','value':'foouser'},{'field':'emails','value':{"email":"foouser2@example2.com"}})
result_search_email = findAll()
for a in result_search_email:
    print (a)

#Use pull to remove all non-primary emails
updateSingle('$pull',{'field':'username','value':'foouser'},{'field':'emails','value':{"emails":{"primary":{"$ne":True}}}})
result_search_email = findAll()
for a in result_search_email:
    print (a)

#Insert element into array with push

new_mail= {"email":"fooemail4@exmaple4.com", "primary":False}
updateSingle('$push',{'field':'username','value':'foouser'},{'field':'emails','value':new_mail})
result_search_email = findAll()
for a in result_search_email:
    print (a)

myconn().products.delete_many({})


#Getting default value in case a property doesn't exist
scored_user={
    "name":"John",
    "score":20
}
insert(scored_user)
scored_user={
    "name":"Otacon",
    "score":100
}
insert(scored_user)
scored_user={
    "name":"Meryl",
    "score":70
}
insert(scored_user)
scored_user={
    "name":"Volgin"
}
insert(scored_user)
#Mongo, same as Python, doesn't actively enforce types
scored_user={
    "name":43224
}
insert(scored_user)

total_score=0
scored_users=findAll()
for user in scored_users:
    total_score += user.get("score",0)
print("The total score is %i" %total_score)

myconn().products.delete_many({})
bank_user={
    "name":"John",
    "balance":20
}
insert(bank_user)
bank_user={
    "name":"Otacon",
    "balance":100
}
insert(bank_user)
bank_user={
    "name":"Meryl",
    "balance":70
}
insert(bank_user)
result_search_email = findAll()
for a in result_search_email:
    print (a)
updateSingle('$inc',{'field':'name','value':'John'},{'field':'balance','value':100})
updateSingle('$inc',{'field':'name','value':'John'},{'field':'balance','value':-200})
print(findByCriteria({'criteria':'name','value':'John'}))

print(findAndModify('$inc',{'field':'name','value':'John'},{'field':'balance','value':500}))

myconn().products.delete_many({})

scores_doc = {
 "user":"Potato",
 "scores_weekly":{
 "2011-01":10,
 "2011-02":3,
 "2011-06":20
 },
 "scores_daily":{
 "2011-35":2,
 "2011-59":7,
 "2011-83":15
 },
 "scores_monthly":{
 "2011-09":30,
 "2011-10":43,
 "2011-11":24
 },
 "score_total":123
}
insert(scores_doc)
now = datetime.utcnow()
current_year = now.year
current_month = now.month
current_week = now.isocalendar()[1]
current_day = now.timetuple().tm_yday
myconn().products.update_one({"user":"Potato"},
 {"$inc":{
 "scores_weekly.%s-%s" %(current_year, current_week):24,
 "scores_daily.%s-%s" %(current_year, current_day):24,
 "scores_monthly.%s-%s" %(current_year, current_month):24,
  "score_total":24,
 }
 })

result_search_email = findAll()
for a in result_search_email:
    print (a)