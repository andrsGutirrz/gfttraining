""" An example of how to connect to MongoDB """
import sys
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure


def main():
    """ Connect to MongoDB """
    try:
        #Creating the connection client
        c = MongoClient(host="localhost", port=27017)
        #selecting the db
        myDb = c["test"]
        #Selection the collection (in sql = table)
        myCol = myDb["products"]
        #returning the connection obj
        print("Connected successfully to Mongo")
        return myDb
    except (ConnectionFailure, e):
        sys.stderr.write("Could not connect to MongoDB: %s" % e)
        sys.exit(1)

#import as a module
if __name__ == "__main__":
    main()
