
let employees = [
  {'name':'juan','salary': 200},
  {'name':'andres','salary': 200},
  {'name':'carlos','salary': 200},
  {'name':'david','salary': 200},
  //{'name':'will-i-am','salary': 200},
];

function printArray(data){
  for (employee of data){
    let output = `Name: ${employee.name} |\t Salary: ${employee.salary}`
    console.log(output);
  }
}

printArray(employees);

let employee2 = employees.map(x => ({'name':x.name,'salary':x.salary+2}));

printArray(employee2);
