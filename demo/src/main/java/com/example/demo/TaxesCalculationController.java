package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class TaxesCalculationController {

    @GetMapping("/")
    public ModelAndView showCalculator(){
        final ModelAndView modelandview = new ModelAndView();
        modelandview.setViewName("calculator");
        modelandview.addObject("taxesCalcultation",new TaxesCalculation());
        return modelandview;
    }

    @PostMapping("/")
    public ModelAndView calculate(@Valid final TaxesCalculation taxesCalculation){
        final ModelAndView modelandview = new ModelAndView();
        modelandview.setViewName("calculator");
        modelandview.addObject("taxesCalcultation",taxesCalculation);
        return modelandview;
    }


}
