package com.example.demo;

import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
public class RestTaxesCalculationController {

    @GetMapping("/taxes/{salary}")
    public TaxesCalculation getTaxesCalculation(@PathVariable final BigDecimal salary){
        TaxesCalculation taxesCalculation = new TaxesCalculation();
        taxesCalculation.setSalary(salary);
        return taxesCalculation;
    }

    @PostMapping("/taxes")
    public TaxesCalculation saveTaxesCalculation(@ModelAttribute final TaxesCalculation taxesCalculation){
        //myService.save(taxesCalculation);
        return taxesCalculation;
    }
}
