﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EjercicioEntity
{
    public class Car
    {
        [ForeignKey("Driver")]
        public int Driver { get; set; }
        [ForeignKey("Manufacturer")]
        public Manufacturer Manufacturer { get; set; }
        [Key]
        public string Plate_Number { get; set; }
        public string Color { get; set; }
    }
}
