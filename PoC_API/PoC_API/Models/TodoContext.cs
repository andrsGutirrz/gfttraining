﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EjercicioEntity;
using Microsoft.EntityFrameworkCore;


namespace PoC_API.Models
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options)
           : base(options)
        {
        }

        public DbSet<Driver> TodoItems { get; set; }
        //public DbSet<Car> carItems { get; set; }
    }
}
