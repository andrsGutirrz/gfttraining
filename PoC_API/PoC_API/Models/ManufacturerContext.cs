﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using EjercicioEntity;

namespace PoC_API.Models
{
    public class ManufacturerContext : DbContext
    {
        public ManufacturerContext(DbContextOptions<ManufacturerContext> options)
             : base(options)
        {
        }

        public DbSet<Manufacturer> manufacturerItems { get; set; }
        //public DbSet<Car> carItems { get; set; } ?
    }
}
