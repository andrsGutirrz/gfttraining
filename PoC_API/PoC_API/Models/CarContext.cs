﻿using EjercicioEntity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoC_API.Models
{
    public class CarContext : DbContext
    {
        public CarContext(DbContextOptions<CarContext> options)
           : base(options)
        {
            //poner aca las navegacioens
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            // Use the shadow property as a foreign key
            modelBuilder.Entity<Car>()
                .HasOne(p => p.Manufacturer);

             
        }
        public DbSet<Car> carItems { get; set; }

    }
}
