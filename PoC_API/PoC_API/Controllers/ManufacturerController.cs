﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EjercicioEntity;
using Microsoft.AspNetCore.Mvc;
using PoC_API.Models;
using Microsoft.EntityFrameworkCore;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PoC_API.Controllers
{
    [Route("api/manufacturer")]
    [ApiController]
    public class ManufacturerController : ControllerBase
    {
        private readonly ManufacturerContext _context;

        public ManufacturerController(ManufacturerContext context)
        {
            _context = context;

            if (_context.manufacturerItems.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                _context.manufacturerItems.Add(new Manufacturer
                {
                    Id_Manu = 1,
                    Name = "Ford"
                });
                _context.manufacturerItems.Add(new Manufacturer
                {
                    Id_Manu = 2,
                    Name = "Toyota"
                });
                _context.SaveChanges();
            }


        }

        // GET: api/Todo
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Manufacturer>>> GetTodoItems()
        {
            return await _context.manufacturerItems.ToListAsync();
        }

        // GET: api/Todo/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Manufacturer>> GetTodoItem(int id)
        {
            var todoItem = await _context.manufacturerItems.FindAsync(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            return todoItem;
        }
    }
}


