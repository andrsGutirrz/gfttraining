﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EjercicioEntity;
using Microsoft.AspNetCore.Mvc;
using PoC_API.Models;
using Microsoft.EntityFrameworkCore;
//using System.Data.Entity


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PoC_API.Controllers
{
    [Route("api/cars")]
    [ApiController]
    public class CarController : ControllerBase
    {
        private readonly CarContext _context;

        public CarController(CarContext context, ManufacturerContext mcont)
        {
            _context = context;

            if (!_context.carItems.Any())
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                _context.carItems.Add(new Car
                {
                    Driver = 1,
                    Plate_Number = "123",
                    Color = "Blue",
                    Manufacturer = mcont.manufacturerItems.First()
                });
                _context.carItems.Add(new Car
                {
                    Driver = 2,
                    Plate_Number = "456",
                    Color = "Black",
                    Manufacturer = mcont.manufacturerItems.Last()

                });
                _context.SaveChanges();
            }
            /*
             
             
        public Driver Driver { get; set; }
        public Manufacturer Manufacturer { get; set; }
        public string Plate_Number { get; set; }
        public string Color { get; set; }
             
             
             */
        }

        // GET: api/Todo
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Car>>> GetTodoItems()
        {
            return await _context.carItems.ToListAsync();
        }

        // GET: api/Todo/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Car>> GetTodoItem(int id)
        {
            var todoItem = await _context.carItems.FindAsync(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            return todoItem;
        }
    }
}
