﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EjercicioEntity
{
    public class Manufacturer
    {
        [Key]
        public long Id_Manu { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Car> cars { get; set; }
    }
}
